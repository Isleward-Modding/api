# Isleward API

Containerized API for Isleward data from RethinkDB

## Usage

Run Isleward using `rethink` as the io method first.

Configure the API using environment variables:
* `IWD_DB_HOST`: the hostname of the RethinkDB instance (default: `localhost`)
* `IWD_DB_PORT`: the port of the RethinkDB instance (default: `28015`)
* `IWD_DB_NAME`: the database name to use (default: `live`)
* `IWD_API_PORT`: the port for the API to listen on (default: `4001`)

It's also possible to store them in a `.env` file:
```
IWD_DB_HOST=
IWD_DB_PORT=
IWD_DB_NAME=
IWD_API_PORT=
```

Run the Isleward-API server using `node index.js` or something like `docker run -d -p 4001:4001 --name isleward-api --env-file ./.env registry.gitlab.com/isleward-modding/api:latest`

## API

### `GET /api/character/{name}`

Get a character by name.

Returns an object containing:
* Name
* Level
* Spirit (`class`)
* Portrait (`portrait.x`, `portrait.y`, and possibly the sheet?)
* Skin (`skin.skinId`, `skin.sheetName`, `skin.cell`)
* Equipment (array of item objects)
* Passives (array of passive tree node IDs, the tree is defined in `iwd/src/server/config/passiveTree.js`; not available in-game besides counting stats)
* Prophecies (array of strings; available in-game through leaderboard only)
* Reputation (array of reputation objects containing the `id`, `rep`, and `tier`)

## Todo

* CORS? helmet?
* Request logging
* API keys/authentication (does access need a key?)
* Leaderboard/fetching all characters?
* Documentation (on landing page?)
* Caching? (handled sufficiently by rethinkdb? limits on how often data will be queried again?)
* Ratelimits
