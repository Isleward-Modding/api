// Load .env file
require('dotenv').config();

// Connect to database and run webserver
require('./src/server');
