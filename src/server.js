const express = require('express');
const app = express();

//eslint-disable-next-line no-process-env
const PORT = process.env.IWD_API_PORT || 4001;

const database = require('./database');
const r = database.r;

// Routes
const router = express.Router();

const cleanCharacter = function (char) {
	let inventory = char.components.find(cpn => cpn.type === 'inventory');
	let passives = char.components.find(cpn => cpn.type === 'passives');
	let prophecies = char.components.find(cpn => cpn.type === 'prophecies');
	let reputation = char.components.find(cpn => cpn.type === 'reputation');

	return {
		name: char.name,
		level: char.level,
		class: char.class,
		portrait: char.portrait,
		skin: {
			sheetName: char.sheetName,
			id: char.skinId,
			cell: char.cell
		},
		equipment: inventory.items.filter(i => i.eq),
		passives: passives.selected,
		prophecies: prophecies.list,
		reputation: reputation.list
	};
};

router.get('/character/:name', async (req, res) => {
	let name = req.params.name;

	if (name.length < 3 || name.length > 12) {
		res.status(400);
		res.json({ error: 'Character name must be between 3 and 12 characters' });
		return;
	}
    
	for (let i = 0; i < name.length; i++) {
		let char = name[i].toLowerCase();
		let valid = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];

		if (!valid.includes(char)) {
			res.status(400);
			res.json({ error: 'Character name is invalid' });
			return;
		}
	}

	let result = await r.table('character').get(name).run();

	if (!result) {
		res.status(404);
		res.json({ error: 'Character doesn\'t exist' });
	} else {
		res.status(200);
		res.json(cleanCharacter(result.value));
	}

});

app.use('/api', router);

// Start
const start = async function () {
	await database.init();

	app.listen(PORT, () => {
		// eslint-disable-next-line no-console
		console.log(`Isleward-API server listening on ${PORT}`);
	});
};

start();
