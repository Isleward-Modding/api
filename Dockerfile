FROM node:lts-alpine

WORKDIR /usr/src/isleward-api

COPY . .

RUN npm ci --only=production

EXPOSE 4001

CMD ["node", "index.js"]