// Default tableNames as of Isleward v0.8.6
// TODO: Should we only check that the character table exists?
const tableNames = [
	'character',
	'characterList',
	'stash',
	'skins',
	'login',
	'leaderboard',
	'customMap',
	'customChannels',
	'error',
	'modLog',
	'accountInfo',
	'recipes'
];

//eslint-disable-next-line no-process-env
const DB_HOST = process.env.IWD_DB_HOST || 'localhost';
//eslint-disable-next-line no-process-env
const DB_PORT = process.env.IWD_DB_PORT || 28015;
//eslint-disable-next-line no-process-env
const DB_NAME = process.env.IWD_DB_NAME || 'live';

const r = require('rethinkdbdash')({
	host: DB_HOST,
	port: DB_PORT,
	db: DB_NAME
});

module.exports = {
	r: r,

	init: async function () {
		await this.create();
	},

	create: async function () {
		let dbList = await r.dbList().run();
        
		if (!dbList.includes(DB_NAME)) 
			throw new Error(`Database '${DB_NAME}' doesn't exist. Run Isleward first or check if the right database name is configured.`);

		// Will default to the db provided in the original configuration
		let tableList = await r.tableList().run();
        
		for (let table of tableNames) {
			if (!tableList.includes(table)) 
				throw new Error(`Database exists, but table '${table}' doesn't exist. Run Isleward first or check if the right database name is configured.`);
		}
	}
};
